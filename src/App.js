import React from 'react';
import * as ReactDOM from 'react-dom';

import './App.css';

import { ScheduleComponent, Day, Week, WorkWeek, Month, Agenda, Inject,
   MonthAgenda, TimelineViews, TimelineMonth, 
   ViewsDirective, ViewDirective,
   DragAndDrop, Resize, ResourcesDirective, ResourceDirective,
   RecurrenceEditorComponent } from '@syncfusion/ej2-react-schedule';

import { extend, L10n, isNullOrUndefined, Ajax } from '@syncfusion/ej2-base';

import { loadCldr } from '@syncfusion/ej2-base';
import { setCulture } from '@syncfusion/ej2-base';
import { setCurrencyCode } from '@syncfusion/ej2-base';

import { DropDownListComponent, MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { DateTimePickerComponent } from '@syncfusion/ej2-react-calendars';
import { defaultData } from './datasource.js';



// import * as numberingSystems from '../numberingSystems.json';
// import * as gregorian from '../ca-gregorian.json';
// import * as numbers from '../numbers.json';
// import * as timeZoneNames from '../timeZoneNames.json';



// import jsonData from './locale.json';
// const loadData = () => JSON.parse(JSON.stringify(jsonData));
// console.log(loadData);

loadCldr(
  require('cldr-data/supplemental/numberingSystems.json'),
  require('cldr-data/main/ko/ca-gregorian.json'),
  require('cldr-data/main/ko/numbers.json'),
  require('cldr-data/main/ko/timeZoneNames.json'));

// loadCldr(
//     require('cldr-data/supplemental/numberingSystems.json'),
//     require('cldr-data/main/fr-CH/ca-gregorian.json'),
//     require('cldr-data/main/fr-CH/numbers.json'),
//     require('cldr-data/main/fr-CH/timeZoneNames.json'));

// loadCldr(numberingSystems, gregorian, numbers, timeZoneNames);


//TODO: 언어를 구분해서... 처리할 것..


// let localeTexts;
// var ajax = new Ajax('/lang/ko.json', 'GET', true);
// ajax.onSuccess = function (value) {
//   localeTexts = value;
//         //Assigning locale text value for Essential JS 2 components
//         L10n.load( JSON.parse(value) );
// };
// ajax.send();

L10n.load({
  'ko': {
      'schedule': {
          'saveButton': '등록',
          'cancelButton': '취소',
          'deleteButton': '삭제',
          'newEvent': '작업 등록',
          'editEvent': '작업 수정',
          'today':'오늘',
          "agenda": "아젠다",
      },

      "recurrenceeditor": {
        "none": "없음",
        "daily": "매일",
        "weekly": "주간",
        "monthly": "월간 간행물",
        "month": "달",
        "yearly": "매년",
        "never": "안함",
        "until": "까지",
        "count": "카운트",
        "first": "먼저",
        "second": "둘째",
        "third": "제삼",
        "fourth": "네번째",
        "last": "마지막",
        "repeat": "반복",
        "repeatEvery": "매번 반복",
        "on": "반복",
        "end": "종료",
        "onDay": "일",
        "days": "일",
        "weeks": "주",
        "months": "월",
        "years": "연령)",
        "every": "...마다",
        "summaryTimes": "타임스)",
        "summaryOn": "에",
        "summaryUntil": "...까지",
        "summaryRepeat": "반복",
        "summaryDay": "일",
        "summaryWeek": "주",
        "summaryMonth": "월",
        "summaryYear": "연령)",
        "monthWeek": "월 주",
        "monthPosition": "월 위치",
        "monthExpander": "월 확장기",
        "yearExpander": "년 확장기",
        "repeatInterval": "간격 반복"
    }

  }
});



setCulture('ko');


class App extends React.Component {

  constructor() {
    super(...arguments);

    this.data = extend([], defaultData, null, true);

    this.ownerData = [
      {OwnerText:'Nancy', Id:1 , OwnerColor: '#ffaa00', designation:'1st floor'},
      {OwnerText:'Steven', Id:2 , OwnerColor: '#f8a398', designation: '2nd floor'},
      {OwnerText:'Michael', Id:3 , OwnerColor: '#7499e1', designation: '3rd floor'}
    ];

    this.fields = { text: 'OwnerText', value: 'Id' };
  }

  onPopupOpen(args) {
    if (args.type == 'Editor') {
        // this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly', 'monthly', 'yearly'];
        console.log(this.scheduleObj.eventWindow);
        console.log(this.scheduleObj.eventWindow.recurrenceEditor);
        // this.scheduleObj.eventWindow.recurrenceEditor.firstDayOfWeek = 1;
           this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly'];
    }
  }

  checkStatus(value){
    if(!isNullOrUndefined(value.Id)){
      let ownerText = [0];
      ownerText[0] = value.OwnerId;
      return ownerText;
    }else{
      // return [this.scheduleObj.getResourcesByIndex(value.groupIndex).resourceData.OwnerText];
    }
  }


//   onPopupOpen(args) {
//     // console.log(args.type);
//     if (args.type === 'Editor') {
//         // this.scheduleObj.eventWindow.recurrenceEditor = this.recurrObject;
//         // this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', '매일', '매주'];
//            this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly'];
//     }
// }


  editorWindowTemplate(props) {

  return (
    props !== undefined && Object.keys(props).length > 0 ?
    <table className="custom-event-editor" style={{ width: '100%', padding: '3' }}>

      <tbody>
        <tr>
          <td colSpan={1} className="e-textlabel">작업</td>
          <td colSpan={2}><input id="Summary" name="Subject" className="e-field e-input" type="text" /></td>
        </tr>

        <tr>
          <td colSpan={1} className="e-textlabel">Owner</td>
          <td colSpan={2}>
            <MultiSelectComponent className="e-field" placeholder="Choose Owner" data-name="OwnerId" dataSource={this.ownerData}
            fields={this.fields} value={this.checkStatus(props)} />
          </td>
        </tr>

        <tr>
          <td colSpan={1} className="e-textlabel">Status</td>
          <td colSpan={2}>
          {/* <DropDownListComponent id="EventType" placeholder='Choose status' data-name="Status" className="e-field" style={{ width: '100%' }} dataSource={['New', 'Requested', 'Confirmed']}> */}

            <DropDownListComponent className="e-field" id="EventType" dataSource={['New', 'Requested', 'Completed']} 
              placeholder='Choose status' data-name='Status' 
              style={{width:'100%'}}>
              </DropDownListComponent>

          </td>
        </tr>

        <tr>
          <td colSpan={1} className="e-textlabel">From</td>
          <td colSpan={2}>
            <DateTimePickerComponent format='yyyy/MM/dd hh:mm a' id="StartTime" data-name="StartTime"
             value={new Date(props.startTime || props.StartTime)} className="e-field"
             ></DateTimePickerComponent>
          </td>
        </tr>

        <tr>
          <td colSpan={1} className="e-textlabel">To</td>
          <td colSpan={2}>
            <DateTimePickerComponent format='yyyy/MM/dd hh:mm a' id="EndTime" data-name="EndTime"
             value={new Date(props.endTime || props.EndTime)} className="e-field"></DateTimePickerComponent>
          </td>
        </tr>

        <tr>
          <td colSpan={1} className="e-textlabel">memo</td>
          <td colSpan={2}><textarea id="Description" className="e-field e-input" name="Description" rows={3} cols={50}
              style={{width:'100%', height:'60px !important', resize: 'vertical' }}></textarea>
          </td>
        </tr>


        <tr><td colSpan={1} className="e-textlabel">Recurrence</td>
          <td colSpan={2}>
          <RecurrenceEditorComponent ref={recurrObject => this.recurrObject = recurrObject} id='RecurrenceEditor'
            frequencies={['none', 'daily', 'weekly']}            
            selectedType='0'/>
        </td></tr>



        {/* <tr>
          <td className="e-textlabel">isAllday</td>
          <td>
            <CheckBoxComponent label="isAllDay"></CheckBoxComponent>
          </td>
        </tr>
 */}

      </tbody>
    </table>
  :''
  );

}


getDoctorLevel(value){
  // return 'a';
  let resourceName = this.getDoctorName(value);
  return (resourceName === 'Nancy') ? '1st floor' : (resourceName === 'Steven') ? '2nd floor' : 
    (resourceName === 'Michael') ? '3rd floor' : '';
}

getDoctorName(value){
  return ((value.resourceData) ? value.resourceData[value.resource.textField] : value.resourceName);
}

resourceHeaderTemplate(props) {
  return (<div className="template-wrap">
  <div className="resource-detail"><div className="resource-name">{this.getDoctorName(props)}</div>
  <div className="resource-designation">{this.getDoctorLevel(props)}</div></div></div>);
}


onActionBegin(args){
  console.log("onActionBegin");
}

onActionComplete(args){
  console.log("onActionComplete");
  console.log(args.requestType);

  // requestType: eventRemoved
  // eventCreated
  // eventChanged
}

onDataBinding(args){
  // console.log("databinding" + args);
  // console.log(args.name);
  // console.log(args.value);
}

//         
// popupOpen={this.onPopupOpen.bind(this)}


  render() {

    console.log("length: " +this.data.length);

    return  (
      <div>
      <ScheduleComponent width='auto' height='auto' 

        locale='ko'

        ref={schedule => this.scheduleObj = schedule}
        selectedDate= {new Date(2020, 0, 14)} 
        allowDragAndDrop={true}
        showQuickInfo={false}
        group={{ resources: ['Owners'] }}
        editorTemplate={this.editorWindowTemplate.bind(this)} 
        resourceHeaderTemplate={this.resourceHeaderTemplate.bind(this)}
        showWeekNumber={true} firstDayOfWeek={1} showWeekend={false}
        
        // actionBegin={this.onActionBegin.bind(this)}
        actionComplete={this.onActionComplete.bind(this)}
        

        // dataBinding={this.onDataBinding.bind(this)}
        


        eventSettings={{ dataSource: this.data,
            fields: {
              id: 'Id',
              subject: { name: 'Subject', title: "작업", validation: {required: true} },
              location : { name: 'Location', title: "위치" },
              description: {name:'Description', title:'메모 '},
              isAllDay: { name: 'IsAllDay', title:'하루종일', validation: {required:true} },
              startTime: { name: 'StartTime', title: '시작시간', validation: {required:true} },
              endTime: { name: 'EndTime', title: '종료시간' } }} 
        }>

        <ResourcesDirective>
            <ResourceDirective field='OwnerId' title='Owner Name' name='Owners' 
              allowMultiple={false} dataSource={this.ownerData} 
              textField='OwnerText' idField='Id' allowGroupEdit={false} colorField='OwnerColor'
              DesignationField='designation' 
              />

        </ResourcesDirective>

      <ViewsDirective dateFormat="yyyy-MM-dd">
        <ViewDirective option='Day' interval={1} datedeleteButtonFormat="yyyy-MM-dd" displayName="1일" />
        <ViewDirective option='Day' interval={3} datedeleteButtonFormat="yyyy-MM-dd" displayName="3일" />
        <ViewDirective option='Week' option='Week' dateFormat="yyyy-MM-dd" displayName="주"/>
        <ViewDirective option='Month' option='Month' showWeekend={false} displayName='월'/>
        <ViewDirective option='Agenda' />
      </ViewsDirective>


        <Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, DragAndDrop,
          Resize ]} />
      </ScheduleComponent>      
    </div>
    );
  }
}

{/* <ScheduleComponent height='750px' selectedDate= {new Date(2020, 0, 14)} eventSettings={ { dataSource: this.data } }>
<Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, TimelineMonth ]} />
</ScheduleComponent>       */}

// function App() {
//   return (
//     <div>
//       <ScheduleComponent height='750px' selectedDate= {new Date(2020, 0, 14)} eventSettings={ { dataSource: data } }>
//         <Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, TimelineMonth ]} />
//       </ScheduleComponent>      
//     </div>
//   );
// }

export default App;

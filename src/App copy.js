import React from 'react';
import * as ReactDOM from 'react-dom';
import logo from './logo.svg';
import './App.css';
import { ScheduleComponent, Day, Week, WorkWeek, Month, Agenda,
   MonthAgenda, TimelineViews, TimelineMonth, Inject,
   ViewsDirective, ViewDirective,
   DragAndDrop, Resize, ResourcesDirective, ResourceDirective,
   RecurrenceEditorComponent } from '@syncfusion/ej2-react-schedule';

import { extend, L10n, isNullOrUndefined } from '@syncfusion/ej2-base';

import { DropDownListComponent, MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { DateTimePickerComponent } from '@syncfusion/ej2-react-calendars';
// import { CheckBoxComponent } from '@syncfusion/ej2-react-buttons';



L10n.load({
  'en-US': {
      'schedule': {
          'saveButton': '등록',
          'cancelButton': '취소',
          'deleteButton': '제거',
          'newEvent': '작업 등록',
          'today':'오늘'
      },
  }
});

  


class App extends React.Component {
  

  data = [
    {
    Id: 1,
    Subject: 'Meeting - 1',
    EVentType: 'Completed',
    StartTime: new Date(2020, 0, 14, 10, 0),
    EndTime: new Date(2020, 0, 14, 12, 30),
    IsAllDay: false,
    OwnerId:1
    },
    {
      Id: 2,
      Subject: 'Meeting',
      EventType: 'New',
      StartTime: new Date(2020, 0, 15, 10, 0),
      EndTime: new Date(2020, 0, 15, 12, 30),
      IsAllDay: false,
      Status: 'Completed',
      Priority: 'High',
      OwnerId:2
    }
];


  constructor() {
    super(...arguments);

    this.ownerData = [
      {OwnerText:'Nancy', Id:1 , OwnerColor: '#ffaa00', designation:'1st floor'},
      {OwnerText:'Steven', Id:2 , OwnerColor: '#f8a398', designation: '2nd floor'},
      {OwnerText:'Michael', Id:3 , OwnerColor: '#7499e1', designation: '3rd floor'}
    ];

    this.fields = { text: 'OwnerText', value: 'Id' };
  }

  onPopupOpen(args) {
    if (args.type == 'Editor') {
        // this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly', 'monthly', 'yearly'];
        console.log(this.scheduleObj.eventWindow);
        console.log(this.scheduleObj.eventWindow.recurrenceEditor);
        this.scheduleObj.eventWindow.recurrenceEditor.firstDayOfWeek = 1;
        
    }
  }

  checkStatus(value){
    if(!isNullOrUndefined(value.Id)){
      let ownerText = [0];
      ownerText[0] = value.OwnerId;
      return ownerText;
    }else{
      // return [this.scheduleObj.getResourcesByIndex(value.groupIndex).resourceData.OwnerText];
    }
  }


//   onPopupOpen(args) {
//     // console.log(args.type);
//     if (args.type === 'Editor') {
//         // this.scheduleObj.eventWindow.recurrenceEditor = this.recurrObject;
//         // this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', '매일', '매주'];
//            this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly'];
//     }
// }






  editorWindowTemplate(props) {

  return (
    props !== undefined && Object.keys(props).length > 0 ?

    <table className="custom-event-editor" style={{ width: '100%', padding: '5' }}>

      <tbody>
        <tr>
          <td className="e-textlabel" >작업</td>
          <td colSpan={4}><input id="Summary" name="Subject" className="e-field e-input" type="text" style={{width:'100%'}}/></td>
        </tr>

        <tr>
          <td className="e-textlabel">Owner</td>
          <td colSpan={4}>
            <MultiSelectComponent className="e-field" placeholder="Choose Owner" data-name="OwnerId" dataSource={this.ownerData}
            fields={this.fields} value={this.checkStatus(props)} />
          </td>
        </tr>

        <tr>
          <td className="e-textlabel">Status</td>
          <td colSpan={4}>
          {/* <DropDownListComponent id="EventType" placeholder='Choose status' data-name="Status" className="e-field" style={{ width: '100%' }} dataSource={['New', 'Requested', 'Confirmed']}> */}

            <DropDownListComponent className="e-field" id="EventType" dataSource={['New', 'Requested', 'Completed']} 
              placeholder='Choose status' data-name='Status' 
              style={{width:'100%'}}>
              </DropDownListComponent>

          </td>
        </tr>

        <tr>
          <td className="e-textlabel">From</td>
          <td colSpan={4}>
            <DateTimePickerComponent format='yyyy/MM/dd hh:mm a' id="StartTime" data-name="StartTime"
             value={new Date(props.startTime || props.StartTime)} className="e-field"
             ></DateTimePickerComponent>
          </td>
        </tr>

        <tr>
          <td className="e-textlabel">To</td>
          <td colSpan={4}>
            <DateTimePickerComponent format='yyyy/MM/dd hh:mm a' id="EndTime" data-name="EndTime"
             value={new Date(props.endTime || props.EndTime)} className="e-field"></DateTimePickerComponent>
          </td>
        </tr>

        <tr>
          <td className="e-textlabel">memo</td>
          <td colSpan={4}><textarea id="Description" className="e-field e-input" name="Description" rows={3} cols={50}
              style={{width:'100%', height:'60px !important', resize: 'vertical' }}></textarea>
          </td>
        </tr>


        <tr><td className="e-textlabel">Recurrence</td><td colSpan={4}>
          <RecurrenceEditorComponent ref={recurrObject => this.recurrObject = recurrObject} id='RecurrenceEditor'></RecurrenceEditorComponent>
        </td></tr>



        {/* <tr>
          <td className="e-textlabel">isAllday</td>
          <td>
            <CheckBoxComponent label="isAllDay"></CheckBoxComponent>
          </td>
        </tr>
 */}

      </tbody>
    </table>
  :''
  );

}


getDoctorLevel(value){
  // return 'a';
  let resourceName = this.getDoctorName(value);
  return (resourceName === 'Nancy') ? '1st floor' : (resourceName === 'Steven') ? '2nd floor' : 
    (resourceName === 'Michael') ? '3rd floor' : '';
}

getDoctorName(value){
  return ((value.resourceData) ? value.resourceData[value.resource.textField] : value.resourceName);
}

resourceHeaderTemplate(props) {
  return (<div className="template-wrap">
  <div className="resource-detail"><div className="resource-name">{this.getDoctorName(props)}</div>
  <div className="resource-designation">{this.getDoctorLevel(props)}</div></div></div>);
}



// 
  render() {

    return  (
      <div>
      <ScheduleComponent width='auto' height='750px' 
        ref={schedule => this.scheduleObj = schedule}
        selectedDate= {new Date(2020, 0, 14)} 
        allowDragAndDrop={true} editorTemplate={this.editorWindowTemplate.bind(this)} 
        showQuickInfo={false} group={{ resources: ['Owners'] }}
        popupOpen={this.onPopupOpen.bind(this)} resourceHeaderTemplate={this.resourceHeaderTemplate.bind(this)}
        showWeekNumber={true} firstDayOfWeek={1} showWeekend={false}
        eventSettings={{ dataSource: this.data,
            fields: {
              id: 'Id',
              subject: { name: 'Subject', title: "작업", validation: {required: true} },
              location : { name: 'Location', title: "위치" },
              description: {name:'Description', title:'메모 '},
              isAllDay: { name: 'IsAllDay', title:'하루종일', validation: {required:true} },
              startTime: { name: 'StartTime', title: '시작시간', validation: {required:true} },
              endTime: { name: 'EndTime', title: '종료시간' } }} 
        }>

        <ResourcesDirective>
            <ResourceDirective field='OwnerId' title='Owner Name' name='Owners' 
              allowMultiple={false} dataSource={this.ownerData} 
              textField='OwnerText' idField='Id' allowGroupEdit={false} colorField='OwnerColor'
              DesignationField='designation' 
              />

        </ResourcesDirective>

      <ViewsDirective dateFormat="yyyy-MM-dd">
        <ViewDirective option='Day' interval={3} dateFormat="yyyy-MM-dd" displayName="3일" />
        <ViewDirective option='Week' option='Week' dateFormat="yyyy-MM-dd" displayName="주"/>
        <ViewDirective option='Month' option='Month' showWeekend={false} displayName='월'/>
        <ViewDirective option='Agenda' />
      </ViewsDirective>


        <Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, DragAndDrop,
          Resize ]} />
      </ScheduleComponent>      
    </div>
    );
  }
}

{/* <ScheduleComponent height='750px' selectedDate= {new Date(2020, 0, 14)} eventSettings={ { dataSource: this.data } }>
<Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, TimelineMonth ]} />
</ScheduleComponent>       */}

// function App() {
//   return (
//     <div>
//       <ScheduleComponent height='750px' selectedDate= {new Date(2020, 0, 14)} eventSettings={ { dataSource: data } }>
//         <Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, TimelineMonth ]} />
//       </ScheduleComponent>      
//     </div>
//   );
// }

export default App;

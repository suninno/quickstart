import React from 'react';
import * as ReactDOM from 'react-dom';

import './App.css';
import { ScheduleComponent, Day, Week, WorkWeek, Month, Agenda, Inject,
   MonthAgenda, TimelineViews, TimelineMonth, 
   ViewsDirective, ViewDirective,
   DragAndDrop, Resize, ResourcesDirective, ResourceDirective,
   RecurrenceEditorComponent } from '@syncfusion/ej2-react-schedule';

import { extend, L10n, isNullOrUndefined, Ajax } from '@syncfusion/ej2-base';

import { loadCldr } from '@syncfusion/ej2-base';
import { setCulture } from '@syncfusion/ej2-base';
import { setCurrencyCode } from '@syncfusion/ej2-base';

import { DropDownListComponent, MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { DateTimePickerComponent } from '@syncfusion/ej2-react-calendars';
// import { CheckBoxComponent } from '@syncfusion/ej2-react-buttons';

import { defaultData } from './datasource.js';



// import * as numberingSystems from '../numberingSystems.json';
// import * as gregorian from '../ca-gregorian.json';
// import * as numbers from '../numbers.json';
// import * as timeZoneNames from '../timeZoneNames.json';



// import jsonData from './locale.json';
// const loadData = () => JSON.parse(JSON.stringify(jsonData));
// console.log(loadData);

loadCldr(
  require('cldr-data/supplemental/numberingSystems.json'),
  require('cldr-data/main/fr-CH/ca-gregorian.json'),
  require('cldr-data/main/fr-CH/numbers.json'),
  require('cldr-data/main/fr-CH/timeZoneNames.json'));

// loadCldr(numberingSystems, gregorian, numbers, timeZoneNames);



// L10n.load({
//   'en-US': {
//       'schedule': {
//           'saveButton': '등록',
//           'cancelButton': '취소',
//           'deleteButton': '제거',
//           'newEvent': '작업 등록',
//           'today':'오늘'
//       },
//   }
// });


// let localeTexts;

// setCulture('ko');
// var ajax = new Ajax('/lang/ko.json', 'GET', true);
// ajax.onSuccess = function (value) {
//   localeTexts = value;
//         //Assigning locale text value for Essential JS 2 components
//         L10n.load( JSON.parse(value) );
// };
// ajax.send();


// L10n.load( JSON.parse(localeTexts) );


// // let ajax = new Ajax('locale.json', 'GET', false);
// // ajax.onSuccess = (value) => {
// //     localeTexts = value;
// //     console.log("localeTexts:" + localeTexts);
// // };
// // ajax.send();

// var ajax = new Ajax('locale.json', 'GET', true);
// ajax.send().then(
//   function(value){
//     console.log("successfully");

//     localeTexts = value;

//     L10n.load( JSON.parse(value) );

//     console.log(  JSON.parse(value)  );

//   },
//   function(reason){
//     console.log(reason);
//   }
// );

// L10n.load( JSON.parse(localeTexts) );

// setCulture('ko'); X


console.log( "locale: ");
console.log( L10n.locale );
// setCulture('ko-KP');
console.log( "locale: ");
console.log( L10n.locale );

// L10n.locale = 'ko';
L10n.load({
  "en-US": {
  "schedule": {
    "day": "일",
    "week": "주",
    "workWeek": "일주일",
    "month": "달",
    "agenda": "비망록",
    "weekAgenda": "주 일정",
    "workWeekAgenda": "일주일 의제",
    "monthAgenda": "달 의제",
    "today": "오늘",
    "noEvents": "이벤트가 없습니다",
    "emptyContainer": "이 날에는 예정된 행사가 없습니다.",
    "allDay": "하루 종일",
    "start": "스타트",
    "end": "종료",
    "more": "더",
    "close": "닫기",
    "cancel": "취소",
    "noTitle": "(제목 없음)",
    "delete": "지우다",
    "deleteEvent": "이번 행사",
    "deleteMultipleEvent": "여러 이벤트 삭제",
    "selectedItems": "선택된 항목",
    "deleteSeries": "전체 시리즈",
    "edit": "편집하다",
    "editSeries": "전체 시리즈",
    "editEvent": "이번 행사",
    "createEvent": "몹시 떠들어 대다",
    "subject": "제목",
    "addTitle": "제목 추가",
    "moreDetails": "자세한 내용은",
    "save": "구하다",
    "editContent": "시리즈의 약속을 어떻게 바꾸시겠습니까?",
    "deleteContent": "이 이벤트를 삭제 하시겠습니까?",
    "deleteMultipleContent": "선택한 이벤트를 삭제 하시겠습니까?",
    "newEvent": "새로운 이벤트",
    "title": "표제",
    "location": "위치",
    "description": "기술",
    "timezone": "시간대",
    "startTimezone": "시간대 시작",
    "endTimezone": "종료 시간대",
    "repeat": "반복",
    "saveButton": "구하다",
    "cancelButton": "취소",
    "deleteButton": "지우다",
    "recurrence": "회귀",
    "wrongPattern": "반복 패턴이 유효하지 않습니다.",
    "seriesChangeAlert": "이 시리즈의 특정 인스턴스에 대한 변경 사항을 취소하고 전체 시리즈와 다시 일치 시키려고합니까?",
    "createError": "이벤트 기간은 발생 빈도보다 짧아야합니다. 지속 시간을 줄이거 나 반복 이벤트 편집기에서 반복 패턴을 변경하십시오.",
    "sameDayAlert": "같은 날 두 번의 동일한 이벤트가 발생할 수 없습니다.",
    "editRecurrence": "반복 편집",
    "repeats": "반복",
    "alert": "경보",
    "startEndError": "선택한 종료 날짜가 시작 날짜 이전에 발생합니다.",
    "invalidDateError": "입력 한 날짜 값이 유효하지 않습니다.",
    "blockAlert": "차단 된 시간 범위 내에서 이벤트를 예약 할 수 없습니다.",
    "ok": "승인",
    "yes": "예",
    "no": "아니",
    "occurrence": "발생",
    "series": "시리즈",
    "previous": "너무 이른",
    "next": "다음 것",
    "timelineDay": "타임 라인 데이",
    "timelineWeek": "타임 라인 주",
    "timelineWorkWeek": "타임 라인 작업 주간",
    "timelineMonth": "타임 라인 월",
    "timelineYear": "타임 라인",
    "editFollowingEvent": "다음 행사",
    "deleteTitle": "이벤트 삭제",
    "editTitle": "이벤트 수정"
},
"recurrenceeditor": {
    "none": "없음",
    "daily": "매일",
    "weekly": "주간",
    "monthly": "월간 간행물",
    "month": "달",
    "yearly": "매년",
    "never": "안함",
    "until": "까지",
    "count": "카운트",
    "first": "먼저",
    "second": "둘째",
    "third": "제삼",
    "fourth": "네번째",
    "last": "마지막",
    "repeat": "반복",
    "repeatEvery": "매번 반복",
    "on": "반복",
    "end": "종료",
    "onDay": "일",
    "days": "일",
    "weeks": "주",
    "months": "월",
    "years": "연령)",
    "every": "...마다",
    "summaryTimes": "타임스)",
    "summaryOn": "에",
    "summaryUntil": "...까지",
    "summaryRepeat": "반복",
    "summaryDay": "일",
    "summaryWeek": "주",
    "summaryMonth": "월",
    "summaryYear": "연령)",
    "monthWeek": "월 주",
    "monthPosition": "월 위치",
    "monthExpander": "월 확장기",
    "yearExpander": "년 확장기",
    "repeatInterval": "간격 반복"
}

  }
});



class App extends React.Component {

  constructor() {
    super(...arguments);

    this.data = extend([], defaultData, null, true);

    this.ownerData = [
      {OwnerText:'Nancy', Id:1 , OwnerColor: '#ffaa00', designation:'1st floor'},
      {OwnerText:'Steven', Id:2 , OwnerColor: '#f8a398', designation: '2nd floor'},
      {OwnerText:'Michael', Id:3 , OwnerColor: '#7499e1', designation: '3rd floor'}
    ];

    this.fields = { text: 'OwnerText', value: 'Id' };
  }

  onPopupOpen(args) {
    if (args.type == 'Editor') {
        // this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly', 'monthly', 'yearly'];
        console.log(this.scheduleObj.eventWindow);
        console.log(this.scheduleObj.eventWindow.recurrenceEditor);
        // this.scheduleObj.eventWindow.recurrenceEditor.firstDayOfWeek = 1;
           this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly'];
    }
  }

  checkStatus(value){
    if(!isNullOrUndefined(value.Id)){
      let ownerText = [0];
      ownerText[0] = value.OwnerId;
      return ownerText;
    }else{
      // return [this.scheduleObj.getResourcesByIndex(value.groupIndex).resourceData.OwnerText];
    }
  }


//   onPopupOpen(args) {
//     // console.log(args.type);
//     if (args.type === 'Editor') {
//         // this.scheduleObj.eventWindow.recurrenceEditor = this.recurrObject;
//         // this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', '매일', '매주'];
//            this.scheduleObj.eventWindow.recurrenceEditor.frequencies = ['none', 'daily', 'weekly'];
//     }
// }


  editorWindowTemplate(props) {

  return (
    props !== undefined && Object.keys(props).length > 0 ?

    <table className="custom-event-editor" style={{ width: '100%', padding: '5' }}>

      <tbody>
        <tr>
          <td className="e-textlabel" >작업</td>
          <td colSpan={4}><input id="Summary" name="Subject" className="e-field e-input" type="text" style={{width:'100%'}}/></td>
        </tr>

        <tr>
          <td className="e-textlabel">Owner</td>
          <td colSpan={4}>
            <MultiSelectComponent className="e-field" placeholder="Choose Owner" data-name="OwnerId" dataSource={this.ownerData}
            fields={this.fields} value={this.checkStatus(props)} />
          </td>
        </tr>

        <tr>
          <td className="e-textlabel">Status</td>
          <td colSpan={4}>
          {/* <DropDownListComponent id="EventType" placeholder='Choose status' data-name="Status" className="e-field" style={{ width: '100%' }} dataSource={['New', 'Requested', 'Confirmed']}> */}

            <DropDownListComponent className="e-field" id="EventType" dataSource={['New', 'Requested', 'Completed']} 
              placeholder='Choose status' data-name='Status' 
              style={{width:'100%'}}>
              </DropDownListComponent>

          </td>
        </tr>

        <tr>
          <td className="e-textlabel">From</td>
          <td colSpan={4}>
            <DateTimePickerComponent format='yyyy/MM/dd hh:mm a' id="StartTime" data-name="StartTime"
             value={new Date(props.startTime || props.StartTime)} className="e-field"
             ></DateTimePickerComponent>
          </td>
        </tr>

        <tr>
          <td className="e-textlabel">To</td>
          <td colSpan={4}>
            <DateTimePickerComponent format='yyyy/MM/dd hh:mm a' id="EndTime" data-name="EndTime"
             value={new Date(props.endTime || props.EndTime)} className="e-field"></DateTimePickerComponent>
          </td>
        </tr>

        <tr>
          <td className="e-textlabel">memo</td>
          <td colSpan={4}><textarea id="Description" className="e-field e-input" name="Description" rows={3} cols={50}
              style={{width:'100%', height:'60px !important', resize: 'vertical' }}></textarea>
          </td>
        </tr>


        <tr><td className="e-textlabel">Recurrence</td><td colSpan={4}>
          <RecurrenceEditorComponent ref={recurrObject => this.recurrObject = recurrObject} id='RecurrenceEditor'
            frequencies={['none', 'daily', 'weekly']}
            
            selectedType='1'
        
          ></RecurrenceEditorComponent>
        </td></tr>



        {/* <tr>
          <td className="e-textlabel">isAllday</td>
          <td>
            <CheckBoxComponent label="isAllDay"></CheckBoxComponent>
          </td>
        </tr>
 */}

      </tbody>
    </table>
  :''
  );

}


getDoctorLevel(value){
  // return 'a';
  let resourceName = this.getDoctorName(value);
  return (resourceName === 'Nancy') ? '1st floor' : (resourceName === 'Steven') ? '2nd floor' : 
    (resourceName === 'Michael') ? '3rd floor' : '';
}

getDoctorName(value){
  return ((value.resourceData) ? value.resourceData[value.resource.textField] : value.resourceName);
}

resourceHeaderTemplate(props) {
  return (<div className="template-wrap">
  <div className="resource-detail"><div className="resource-name">{this.getDoctorName(props)}</div>
  <div className="resource-designation">{this.getDoctorLevel(props)}</div></div></div>);
}


onActionBegin(args){
  console.log("onActionBegin");
}

onActionComplete(args){
  console.log("onActionComplete");
  console.log(args.requestType);

  // requestType: eventRemoved
  // eventCreated
  // eventChanged
}

onDataBinding(args){
  // console.log("databinding" + args);
  // console.log(args.name);
  // console.log(args.value);

}

//         
// popupOpen={this.onPopupOpen.bind(this)}
// locale='en-US'  O
// locale='fr-CH' O 
// locale='ko-KR' X
// locale="ko" X


  render() {

    console.log("length: " +this.data.length);

    return  (
      <div>
      <ScheduleComponent width='auto' height='750px' 


        ref={schedule => this.scheduleObj = schedule}
        selectedDate= {new Date(2020, 0, 14)} 
        allowDragAndDrop={true}
        showQuickInfo={false}
        group={{ resources: ['Owners'] }}
        editorTemplate={this.editorWindowTemplate.bind(this)} 
        resourceHeaderTemplate={this.resourceHeaderTemplate.bind(this)}
        showWeekNumber={true} firstDayOfWeek={1} showWeekend={false}
        
        // actionBegin={this.onActionBegin.bind(this)}
        actionComplete={this.onActionComplete.bind(this)}
        

        // dataBinding={this.onDataBinding.bind(this)}
        


        eventSettings={{ dataSource: this.data,
            fields: {
              id: 'Id',
              subject: { name: 'Subject', title: "작업", validation: {required: true} },
              location : { name: 'Location', title: "위치" },
              description: {name:'Description', title:'메모 '},
              isAllDay: { name: 'IsAllDay', title:'하루종일', validation: {required:true} },
              startTime: { name: 'StartTime', title: '시작시간', validation: {required:true} },
              endTime: { name: 'EndTime', title: '종료시간' } }} 
        }>

        <ResourcesDirective>
            <ResourceDirective field='OwnerId' title='Owner Name' name='Owners' 
              allowMultiple={false} dataSource={this.ownerData} 
              textField='OwnerText' idField='Id' allowGroupEdit={false} colorField='OwnerColor'
              DesignationField='designation' 
              />

        </ResourcesDirective>

      <ViewsDirective dateFormat="yyyy-MM-dd">
        <ViewDirective option='Day' interval={3} dateFormat="yyyy-MM-dd" displayName="3일" />
        <ViewDirective option='Week' option='Week' dateFormat="yyyy-MM-dd" displayName="주"/>
        <ViewDirective option='Month' option='Month' showWeekend={false} displayName='월'/>
        <ViewDirective option='Agenda' />
      </ViewsDirective>


        <Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, DragAndDrop,
          Resize ]} />
      </ScheduleComponent>      
    </div>
    );
  }
}

{/* <ScheduleComponent height='750px' selectedDate= {new Date(2020, 0, 14)} eventSettings={ { dataSource: this.data } }>
<Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, TimelineMonth ]} />
</ScheduleComponent>       */}

// function App() {
//   return (
//     <div>
//       <ScheduleComponent height='750px' selectedDate= {new Date(2020, 0, 14)} eventSettings={ { dataSource: data } }>
//         <Inject services={[Day, Week, WorkWeek, Month, Agenda, MonthAgenda, TimelineViews, TimelineMonth ]} />
//       </ScheduleComponent>      
//     </div>
//   );
// }

export default App;

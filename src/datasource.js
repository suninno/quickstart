


export let defaultData = [
    {
    Id: 1,
    Subject: 'Meeting - 1',
    EVentType: 'Completed',
    StartTime: new Date(2020, 0, 15, 10, 0),
    EndTime: new Date(2020, 0, 15, 12, 30),
    IsAllDay: false,
    OwnerId:1
    },
    {
      Id: 2,
      Subject: 'Meeting',
      EventType: 'New',
      StartTime: new Date(2020, 0, 15, 10, 0),
      EndTime: new Date(2020, 0, 15, 12, 30),
      IsAllDay: false,
      Status: 'Completed',
      Priority: 'High',
      OwnerId:2
    }
];


